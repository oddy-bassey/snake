package com.vaultcode.component;

public enum Direction {

    RIGHT,
    LEFT,
    UP,
    DOWN
}
