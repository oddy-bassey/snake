package com.vaultcode.component;

import com.vaultcode.listener.GameKeyListener;

import java.awt.*;

public abstract class Scene {

    public abstract void update(double deltaTime);
    public abstract void draw(Graphics graphics);
}
