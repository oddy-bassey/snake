package com.vaultcode.component;

import com.vaultcode.frame.Window;
import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.listener.GameMouseListener;
import com.vaultcode.util.Constants;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Logger;

public class MenuScene extends Scene{

    private static Logger log = Logger.getLogger(MenuScene.class.getName());
    public BufferedImage title, play, playPressed, exit, exitPressed;
    public BufferedImage playCurrentImage, exitCurrentImage, snake;
    public Rect titleRect, playRect, exitRect;
    private GameMouseListener mouseListener;

    public MenuScene(GameMouseListener mouseListener) {
        this.mouseListener = mouseListener;

        try {
            BufferedImage spriteSheet = ImageIO.read(new File("assets/menuSprite.png"));
            BufferedImage snakeSprite = ImageIO.read(new File("assets/snakeSprite.png"));

            title = spriteSheet.getSubimage(0, 242, 960, 240);
            play = spriteSheet.getSubimage(0, 121, 261, 121);
            playPressed = spriteSheet.getSubimage(264, 121, 261, 121);
            exit = spriteSheet.getSubimage(0, 0, 233, 93);
            exitPressed = spriteSheet.getSubimage(264, 0, 233, 93);

            snake = snakeSprite.getSubimage(0, 0, 610, 526);
        } catch (Exception exception) {
            log.severe(exception.getMessage());
        }

        playCurrentImage = play;
        exitCurrentImage = exit;

        titleRect = new Rect(240, 100, 250, 80);
        playRect = new Rect(310, 280, 130, 50);
        exitRect = new Rect(318, 355, 110, 35);
    }

    @Override
    public void update(double deltaTime) {
        if(mouseListener.getX() >= playRect.getX() && mouseListener.getX() <= playRect.getX() + playRect.getWidth() &&
                mouseListener.getY() >= playRect.getY() && mouseListener.getY() <= playRect.getY() + playRect.getHeight()) {
            playCurrentImage = playPressed;

            if(mouseListener.isPressed()) {
                Window.getWindow().changeState(1);
            }
        } else {
            playCurrentImage = play;
        }

        if(mouseListener.getX() >= exitRect.getX() && mouseListener.getX() <= exitRect.getX() + exitRect.getWidth() &&
                mouseListener.getY() >= exitRect.getY() && mouseListener.getY() <= exitRect.getY() + exitRect.getHeight()) {
            exitCurrentImage = exitPressed;

            if(mouseListener.isPressed()) {
                Window.getWindow().close();
            }
        } else {
            exitCurrentImage = exit;
        }
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(new Color(37, 35, 35, 255));
        graphics.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        graphics.drawImage(title, (int) titleRect.getX(), (int) titleRect.getY(), (int) titleRect.getWidth(), (int) titleRect.getHeight(), null);
        graphics.drawImage(snake, 245 + (int) titleRect.getWidth(), 90, 150, 100, null);
        graphics.drawImage(playCurrentImage, (int) playRect.getX(), (int) playRect.getY(), (int) playRect.getWidth(), (int) playRect.getHeight(), null);
        graphics.drawImage(exitCurrentImage, (int) exitRect.getX(), (int) exitRect.getY(), (int) exitRect.getWidth(), (int) exitRect.getHeight(), null);
    }
}
