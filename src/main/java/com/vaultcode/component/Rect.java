package com.vaultcode.component;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Rect {

    private double x;
    private double y;
    private double width;
    private double height;
}
