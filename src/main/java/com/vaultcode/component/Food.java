package com.vaultcode.component;

import com.vaultcode.util.Constants;

import java.awt.*;
import java.util.logging.Logger;

public class Food {
    private final Logger log = Logger.getLogger(getClass().getName());
    public Rect background;
    public Snake snake;
    public int width, height;
    public Color color;
    public int xPadding;
    public Rect food;
    public boolean isSpawned = false;

    public Food(Rect background, Snake snake, int width, int height, Color color) {
        this.background = background;
        this.snake = snake;
        this.width = width;
        this.height = height;
        this.color = color;
        this.food = new Rect(0, 0, width, height);

        xPadding = (int) ((Constants.TILE_WIDTH - this.width) / 2.0);
    }

    public void spawn() {
        do {
            double randX = (int) (Math.random() * (int) (background.getWidth() / Constants.TILE_WIDTH)) * Constants.TILE_WIDTH + background.getX();
            double randY = (int) (Math.random() * (int) (background.getHeight() / Constants.TILE_WIDTH)) * Constants.TILE_WIDTH + background.getY();
            this.food.setX(randX);
            this.food.setY(randY);
        } while(this.snake.intersectingWithRect(this.food));
        this.isSpawned = true;
    }

    public void update(double deltaTime) {
        if(this.snake.intersectingWithRect(this.food)) {
            this.snake.grow();
            this.food.setX(-100);
            this.food.setY(-100);
            this.isSpawned = false;
        }
    }

    public void draw(Graphics2D graphics2D) {
        graphics2D.setColor(color);
        graphics2D.fillRect((int)(this.food.getX() + xPadding), (int)(this.food.getY() + xPadding), width, height);
    }
}
