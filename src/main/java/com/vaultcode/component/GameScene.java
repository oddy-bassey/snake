package com.vaultcode.component;

import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.util.Constants;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;

public class GameScene extends Scene {

    private Rect background, foreground;
    private Snake snake;
    private GameKeyListener keyListener;
    private Food food;

    public GameScene(GameKeyListener keyListener) {
        this.keyListener = keyListener;
        background = new Rect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        foreground = new Rect(24, 48, 750, 528); // 750, 528
        snake = new Snake(foreground, 8, 48, 48 + 24, 24, 24);
        food = new Food(foreground, snake, 12, 12, Color.WHITE);
        food.spawn();
    }

    @Override
    public void update(double deltaTime) {
        if(keyListener.isKeyPressed(KeyEvent.VK_UP)) {
            snake.changeDirection(Direction.UP);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_DOWN)) {
            snake.changeDirection(Direction.DOWN);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_RIGHT)) {
            snake.changeDirection(Direction.RIGHT);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_LEFT)) {
            snake.changeDirection(Direction.LEFT);
        }

        if(!food.isSpawned) {
            food.spawn();
        }

        food.update(deltaTime);
        snake.update(deltaTime);
    }

    @Override
    public void draw(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D) graphics;

        graphics2D.setColor(new Color(80, 79, 79, 255));
        graphics2D.fill(new Rectangle2D.Double(background.getX(), background.getY(), background.getWidth(), background.getHeight()));

        graphics2D.setColor(new Color(37, 35, 35, 255));
        graphics2D.fill(new Rectangle2D.Double(foreground.getX(), foreground.getY(), foreground.getWidth(), foreground.getHeight()));

        snake.draw(graphics2D);
        food.draw(graphics2D);
    }
}
