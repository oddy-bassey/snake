package com.vaultcode.component;

import com.vaultcode.frame.Window;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

public class Snake {
    private final Logger  log = Logger.getLogger(getClass().getName());
    public Rect[] body = new Rect[100];
    public double bodyWidth, bodyHeight;
    public int size;
    public int tail = 0;
    public int head = 0;
    public double ogWaitBetweenUpdate = 0.1f;
    public double waitTimeLeft = ogWaitBetweenUpdate;
    public Direction direction = Direction.RIGHT;
    public Rect background;

    public Snake(Rect background, int size, int startX, int startY, double bodyWidth, double bodyHeight) {
        this.size = size;
        this.bodyWidth = bodyWidth;
        this.bodyHeight = bodyHeight;
        this.background = background;

        for (int i = 0; i <= size ; i++) {
            Rect bodyPiece = new Rect(startX + (i * bodyWidth), startY, bodyWidth, bodyHeight);
            body[i] = bodyPiece;
            head++;
        }
        head--;
    }

    public void changeDirection(Direction newDirection) {
        if(direction == Direction.RIGHT && newDirection != Direction.LEFT) this.direction = newDirection;
        else if(direction == Direction.LEFT && newDirection != Direction.RIGHT) this.direction = newDirection;
        else if(direction == Direction.UP && newDirection != Direction.DOWN) this.direction = newDirection;
        else if(direction == Direction.DOWN && newDirection != Direction.UP) this.direction = newDirection;
    }

    public boolean isIntersecting(Rect r1, Rect r2) {
        return (r1.getX() >= r2.getX() && r1.getX() + r1.getWidth() <= r2.getX() + r2.getWidth() &&
                r1.getY() >= r2.getY() && r1.getY() + r1.getHeight() <= r2.getY() + r2.getHeight());
    }

    public boolean intersectingWithRect(Rect rect) {
        for (int i = tail; i != head; i = (i+1) % body.length) {
            if (isIntersecting(rect, body[i])) return true;
        }
        return false;
    }

    public boolean intersectingWithSelf() {
        Rect snakeHead = body[head];

        return intersectingWithRect(snakeHead) || intersectingWithBoundaries(snakeHead);
    }

    public boolean intersectingWithBoundaries(Rect snake) {

        return (snake.getX() < background.getX() || snake.getX() > background.getX() + (background.getWidth() - 5.0) ||
                    snake.getY() < background.getY() || snake.getY() > background.getY() + (background.getHeight() - 5.0));
    }

    public void grow() {
        log.info("snake grew!!");
        double newX = 0;
        double newY = 0;

        if(direction == Direction.RIGHT) {
            newX = body[tail].getX() - bodyWidth;
            newY = body[tail].getY();
        } else if(direction == Direction.LEFT) {
            newX = body[tail].getX() + bodyWidth;
            newY = body[tail].getY();
        } else if(direction == Direction.DOWN) {
            newX = body[tail].getX();
            newY = body[tail].getY() - bodyHeight;
        } else if(direction == Direction.UP) {
            newX = body[tail].getX();
            newY = body[tail].getY() + bodyHeight;
        }

        Rect newBodyPiece = new Rect(newX, newY, bodyWidth, bodyHeight);

        tail = (tail - 1) % body.length;
        body[tail] = newBodyPiece;
    }

    public void update(double deltaTime) {
        if(waitTimeLeft > 0) {
            waitTimeLeft -= deltaTime;

            return;
        }

        if(intersectingWithSelf()) {
            Window.getWindow().changeState(0);
        }

        waitTimeLeft = ogWaitBetweenUpdate;
        double newX = 0;
        double newY = 0;

        if(direction == Direction.RIGHT) {
            newX = body[head].getX() + bodyWidth;
            newY = body[head].getY();
        } else if(direction == Direction.LEFT) {
            newX = body[head].getX() - bodyWidth;
            newY = body[head].getY();
        } else if(direction == Direction.DOWN) {
            newX = body[head].getX();
            newY = body[head].getY() + bodyHeight;
        } else if(direction == Direction.UP) {
            newX = body[head].getX();
            newY = body[head].getY() - bodyHeight;
        }

        body[(head + 1) % body.length] = body[tail];
        body[tail] = null;
        head = (head + 1) % body.length;
        tail = (tail + 1) % body.length;

        body[head].setX(newX);
        body[head].setY(newY);
    }

    public void draw(Graphics2D graphics2D) {

        /* Note:
         * Head of the snake is at the end of the array [arraySize-1]
         * Tail of the snake is at the start of the array
        */
        for (int i = tail; i != head; i = (i+1) % body.length) {
            Rect bodyPiece = body[i];
            double subWidth = (bodyPiece.getWidth() - 6.0) / 2;
            double subHeight = (bodyPiece.getHeight() - 6.0) / 2;

            graphics2D.setColor(Color.GREEN);
            graphics2D.fill(new Rectangle2D.Double(bodyPiece.getX() + 2.0, bodyPiece.getY() + 2.0, subWidth, subHeight));
            graphics2D.fill(new Rectangle2D.Double(bodyPiece.getX() + 4.0 + subWidth, bodyPiece.getY() + 2.0, subWidth, subHeight));
            graphics2D.fill(new Rectangle2D.Double(bodyPiece.getX() + 2.0, bodyPiece.getY() + 4.0 + subHeight, subWidth, subHeight));
            graphics2D.fill(new Rectangle2D.Double(bodyPiece.getX() + 4.0 + subWidth, bodyPiece.getY() + 4.0 + subHeight, subWidth, subHeight));
        }
    }
}
