package com.vaultcode.util;

public class Constants {

    public static int SCREEN_HEIGHT = 600;
    public static int SCREEN_WIDTH = 800;
    public static String TITLE = "Snake Game";
    public static final float TILE_WIDTH = 24;
}
