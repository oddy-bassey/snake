package com.vaultcode.listener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener extends KeyAdapter implements KeyListener {

    private boolean[] keyPressed = new boolean[128];
    @Override
    public void keyPressed(KeyEvent event) {
        keyPressed[event.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent event) {
        keyPressed[event.getKeyCode()] = false;
    }

    public boolean isKeyPressed(int keyIndex) {
        return keyPressed[keyIndex];
    }
}
