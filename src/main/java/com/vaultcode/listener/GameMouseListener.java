package com.vaultcode.listener;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

@Data
@NoArgsConstructor
public class GameMouseListener extends MouseAdapter implements MouseMotionListener {

    private boolean isPressed = false;
    private double x = 0.0, y = 0.0;

    @Override
    public void mousePressed(MouseEvent e) {
        this.isPressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.isPressed = false;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
    }
}
