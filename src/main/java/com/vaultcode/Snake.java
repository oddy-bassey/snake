package com.vaultcode;

import com.vaultcode.frame.Window;
import com.vaultcode.util.Constants;

/**
 * Hello world!
 *
 */
public class Snake
{
    public static void main( String[] args ) {
        Window gameWindow = Window.getWindow();

        Thread gameThread = new Thread(gameWindow);
        gameThread.start();
    }
}
