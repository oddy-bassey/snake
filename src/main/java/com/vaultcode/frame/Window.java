package com.vaultcode.frame;

import com.vaultcode.component.GameScene;
import com.vaultcode.component.MenuScene;
import com.vaultcode.component.Scene;
import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.listener.GameMouseListener;
import com.vaultcode.util.Constants;
import com.vaultcode.util.Time;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class Window extends JFrame implements Runnable {
    private static Window window = null;
    private  final Logger log = Logger.getLogger(Window.class.getName());
    private boolean isRunning;
    public  int currentState;
    public  Scene currentScene;
    private  GameMouseListener mouseListener = new GameMouseListener();
    private GameKeyListener keyListener = new GameKeyListener();

    public Window(int width, int height, String title) {
        setSize(width, height);
        setTitle(title);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        isRunning = true;
        addKeyListener(keyListener);
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);

        changeState(0);
    }

    public static Window getWindow() {
        if(window == null) {
            window = new Window(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT, Constants.TITLE);
        }

        return window;
    }

    public  void changeState(int newState) {
        currentState = newState;
        switch (currentState) {
            case 0 -> currentScene = new MenuScene(mouseListener);
            case 1 -> currentScene = new GameScene(keyListener);
            default -> log.severe("Unknown Scene!");
        }
    }

    public  void close() {
        this.isRunning = false;
    }

    public void update(double deltaTime) {
        Image doubleBufferImage = createImage(getWidth(), getHeight());
        Graphics doubleBufferGraphics = doubleBufferImage.getGraphics();

        this.draw(doubleBufferGraphics);
        getGraphics().drawImage(doubleBufferImage, 0, 0, this);

        currentScene.update(deltaTime);
    }

    public void draw(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D) graphics;

        currentScene.draw(graphics);
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;

        try {
            while(isRunning) {
                double time = Time.getTime();
                double delaTime = time - lastFrameTime;
                lastFrameTime = time;

                update(delaTime);
            }
        } catch(Exception exception) {
            log.severe(exception.getMessage());
        }

        this.dispose();
    }
}
